const express = require('express');
const router = express.Router();
const auth = require("../auth");

const userController = require('../controllers/user')

//Route for checking if the user's email already exists in the database
router.post('/checkEmail', (req, res)=>{
	userController.checkEmailExists(req.body).then(result => res.send(result))
})


//Routes for User Registration
router.post('/register', (req, res) => {
	userController.registerUser(req.body).then(result => res.send(result));
})


//Routes for authenticating a user
router.post('/login', (req, res) => {
	userController.loginUser(req.body).then(result => res.send(result))
})

// Route for retrieving user details
/*router.post("/", auth.verify, (req, res) => {

	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	courseController.addCourse(data).then(resultFromController => res.send(resultFromController));

});
*/
router.get("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	// Provides the user's ID for the getProfile controller method
	userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));

});

// Route to enroll user to a course
router.post("/enroll", (req, res) => {
	let data = {
		userId : req.body.userId,
		courseId : req.body.courseId
	}

	userController.enroll(data).then(resultFromController => res.send(resultFromController));
})

// 1. Refactor the user route to implement user authentication for the enroll route.
// 2. Process a POST request at the /enroll route using postman to enroll a user to a course.
router.get("/:userId", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	// Provides the user's ID for the getProfile controller method
	userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));

});


router.post("/enroll", (req, res) => {
	if(data !== {userId : req.body.userId})
	let data = {
		userId : req.body.userId,
		courseId : req.body.courseId
	}

	userController.enroll(data).then(resultFromController => res.send(resultFromController));
}else{
	return (`${userId} is already enrolled.`)
})




module.exports = router;
