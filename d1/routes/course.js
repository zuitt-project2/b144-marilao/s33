const express = require("express");
const router = express.Router();
const auth = require("../auth");

const courseController = require("../controllers/course");

// Route for creating a course
/*router.post("/", (req, res) => {
	courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController))
})*/

/*1. Refactor the course route to implement user authentication for the admin when creating a course.*/

router.post("/", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)

    courseController.addCourse(req.body, {userId: userData.id, isAdmin:userData.isAdmin}).then(resultFromController => res.send(resultFromController))
})
// Retrieve all courses
router.get("/all", (req, res) => {
	courseController.getAllCourses().then(resultFromController => res.send(resultFromController));
})

// Retrieve all active courses
router.get("/", (req,res) => {
	courseController.getAllActive().then(resultFromController => res.send(
		resultFromController));
	})
// Retrieve a specific course
router.get("/:courseId", (req,res) =>{
	console.log(req.params.courseId);

	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));
})
// Update a course with authorization
router.put("/:courseId", auth. verify, (req,res) =>{
	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController))
})

/*1. Create a route for archiving a course. The route must use JWT authentication and obtain the course ID from the url.*/
router.patch("/:courseId", auth.verify, (req, res) => {
courseController.archiveCourse(req.params, req.body)
.then(resultFromController => res.send(resultFromController));
})
/*router.put("/:courseId/archive", auth.verify, (req, res) => {
courseController.archiveCourse(req.params)
.then(resultFromController => res.send(resultFromController));
})*/


module.exports = router;

