/*

	bcrypt = for hashing user passwords prior to storage
	
	cors = for allowing cross-origin resource sharing (just like a security guard); only allowed origins are allowed to access; very scrict that blocks every unknown id

	jsonwebtoken = for implementing JSON web tokens; just like in the movie theater, once you log in, you will get a token, if you dont have, you cant enter

*/

const express = require('express')
const mongoose = require('mongoose')
// Allows us to control app's Cross Origin Resource Sharing settings
const cors = require('cors');

//Routes
const userRoutes = require('./routes/user')

const courseRoutes = require('./routes/course');

// Server Setup
const app = express()
const port = 4000;
// allows all resources/origin to access our backend application
// Enable all CORS
app.use(cors())

app.use(express.json())
app.use(express.urlencoded({extended : true}))

//Defines the 'api/users' string to be included for all routes defined in the 'user' route file
app.use('/api/users', userRoutes)
app.use('/api/courses', courseRoutes)


// Database connection
mongoose.connect("mongodb+srv://Shiela1028:JesusChrist777@wdc028-course-booking.tflhi.mongodb.net/batch144_booking_system?retryWrites=true&w=majority", {
	// to avoid/prevents future error in our connection to MongoDB
	useNewUrlParser: true, 
	useUnifiedTopology: true
})

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"))
db.once("open", () => console.log("Now connected to MongoDB Atlas."))




app.listen(port, () => console.log(`Now listening to port ${port}`));